# PCBump

A simple ASP.NET MVC4 C# application using MongoDB as the database.

Used for distributing new PCs and scheduling bumps.

Started off by following Dr. Dobb's [MongoDB with C#: Deep Dive](http://www.drdobbs.com/database/mongodb-with-c-deep-dive/240152181), which was a great help, but then when added MongoRepository, it all became much easier.

## Warning - this is a big mess of a project at this stage - so wouldn't go copying it wholesale - but it does show a proof of concept and perhaps a wonderful thing.

## NuGet packages

1. Official MongoDB C# Driver (1.8.2)
2. MongoRepository (1.5.2)

## Other Features
1. [Twitter Bootstrap](http:twitter.github.io/bootstrap/index.html)
2. [FontAwesome](http://fortawesome.github.io/Font-Awesome/)
3. [TinyMCE](http://www.tinymce.com) - for rich text editing ???needed for this project
4. [jquery](http://jquery.com)
5. [FullCalendar](http://arshaw.com/fullcalendar/) - a jquery full calendar plugin