﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

namespace pcbump.Data {
    using System;
    using System.Collections.Generic;
    using MongoRepository;

    public class PCDCGroup : Entity {
        public string Organization { get; set; }
        public string PCDC { get; set; }
        //and the list of users being maintained
        public List<string> Users { get; set; }

        public PCDCGroup() {
            this.Users = new List<string>();
        }
    }

    public class Reservation : Entity {
        public string Username { get; set; }
        public string Contact { get; set; }
        public int QueuePosition { get; set; }
        public DateTime InstallDateTime { get; set; }
    }
}
