﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MongoRepository;
using pcbump.Data;

namespace pcbump.Controllers {
    public class PCDCGroupController : Controller {
        static MongoRepository<PCDCGroup> pcdcgrouprepo = new MongoRepository<PCDCGroup>();
        //
        // GET: /PCDCGroup/

        public ActionResult Index()
        {
            return View(pcdcgrouprepo);
        }

        //
        // GET: /PCDCGroup/Details/5

        public ActionResult Details(string id)
        {
            var pcdcgroup = pcdcgrouprepo.GetById(id);
            return View(pcdcgroup);
        }

        //
        // GET: /PCDCGroup/Create
        public ActionResult Create()
        {
            return View(new PCDCGroup());
        }

        //
        // POST: /PCDCGroup/Create
        [HttpPost]
        //public ActionResult Create(FormCollection collection) {
        public ActionResult Create(PCDCGroup pcdcgroup) {
            try {
                pcdcgrouprepo.Add(pcdcgroup);

                return RedirectToAction("Index");
            } catch {
                return View();
            }
        }

        //
        // GET: /PCDCGroup/Edit/5

        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /PCDCGroup/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /PCDCGroup/Delete/5

        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /PCDCGroup/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
