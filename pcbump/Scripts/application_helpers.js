﻿var application_helpers = {};
application_helpers.resetItemClass = function (parentSelector, activeSelector, classname) {
    //given a parentSelector will spin through all children of the parent item, remove the specified class from all elements
    //then set class = active to the correct item
    //used in the navigator to set the appropriate active navigator item
    $(parentSelector).children().removeClass(classname);
    $(activeSelector).addClass(classname);
}

